const Discord = require('discord.js')
const bot = new Discord.Client();

const token = 'NjI5Mzk1OTYzNzk4MzU1OTc4.XZZJBA.WMrAM9lzACSXMGcFb0uAyPLjZyk';

const PREFIX = "/";

bot.on('ready', () =>{
    console.log('This bot is now online');
})

bot.on('message', message=>{
    let args = message.content.substring(PREFIX.length).split(" ");
    switch(args[0]){
        case 'support':
            const support = new Discord.RichEmbed()
            .setTitle('**Support**')
            .setColor(0x00E7FF)
            .setDescription('If you have any questions then feel free to message a staff member.')
            message.channel.send(support);
            break;
        case 'apply':
            const apply = new Discord.RichEmbed()
            .setTitle('**Apply**')
            .setColor(0x00E7FF)
            .setDescription('Apply Here - https://docs.google.com/forms/d/16kfHCg15rXJbdnXPID4AVryBRqXYK7bb6eongHQ4RWM/edit')
            message.channel.send(apply);
            break;
        case 'help':
            const help = new Discord.RichEmbed()
            .setTitle('**Commands**')
            .setColor(0x00E7FF)
            .setDescription('Here is a list of my commands:')
            .addField("/help", 'Allows you to see the commands that you have access to')
            .addField("/apply", 'Provides a link to the staff application')
            .addField("/discord", 'Provides a direct link to the discord server')
            .addField("/support", 'Provides information on how to receive assistance')
            message.channel.send(help)
            break;
        case 'discord':
            const discord = new Discord.RichEmbed()
            .setTitle("**Discord**")
            .setColor(0x00E7FF)
            .setDescription('Link - https://discord.gg/YSvrSxv')
            message.channel.send(discord)
            break;
        case 'staffhelp':
            const staffhelp = new Discord.RichEmbed()
            .setTitle('**Staff Commands**')
            .setColor(0x00E7FF)
            .setDescription('Provides a list of commands only staff can access')
            .addField("/stafflist", 'Provides a list of the Staff Members')
            .addField("/staffguide", 'Provides a link to the staff guide')
            message.channel.send(staffhelp)
            break;
        case 'stafflist':
            const stafflist = new Discord.RichEmbed()
            .setTitle('**Staff List**')
            .setColor(0x00E7FF)
            .setDescription('Current Staff Members:')
            .addField("__**Owner**__", 'Jack')
            .addField("__**Head Administrator**__", 'Sewing')
            .addField("__**Senior Administrator**__", 'Mat')
            .addField("__**Administrator**__", 'Alex')
            .addField("__**Senior Staff**__", "Xavier")
            .addField("__**Staff**__", 'RandenBerg, Sam, Caleb')
            .addField("__**Trial Staff**__", 'Teeqzy')
            message.channel.send(stafflist)
            break;
        case 'staffguide':
            const staffguide = new Discord.RichEmbed()
            .setTitle('**Staff Guide**')
            .setColor(0x00E7FF)
            .setDescription('Link - https://docs.google.com/document/d/1CsrHtMefBlNchBplbV_ASZOOUojf8nmQgPY9s4wvnT8/edit?usp=sharing')
            message.channel.send(staffguide)
            break;
            
let messageArray = message.content.split(" ");
let cmd = messageArray[0];
let args = messageArray.slice(1);
    if(cmd === '$[PREFIX]kick'){

    let kUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
    if(!kUser) return message.channel.send("Cannot find user.");
    let kReason = args.join(" ").slice(22);
    if(!message.member.hasPermission("KICK_MEMBERS")) return message.channel.send("Insufficient Permissions.");
    if(kUser.hasPermission("KICK_MEMBERS")) return message.channel.send("That person cannot be kicked.");

    let kickEmbed = new Discord.RichEmbed()
    .setDescription("**Kick**")
    .setColor(0x00E7FF)
    .addField("Kicked User:", '${kUser} with ID ${message.author.id}')
    .addField("Kicked By:", '<@${message.author.id with ID ${message.author.id}')
    .addField("Time:", message.createdAt)
    .addField("Reason:", kReason);

    let kickChannel = message.guild.channels.find('name', "logs");
    if(!kickChannel) return message.channel.send("Unable to find channel.")

    message.guild.member(kUser).kick(kReason)
    kickChannel.send(kickEmbed);
    break;
}
    }
})
bot.login(token);